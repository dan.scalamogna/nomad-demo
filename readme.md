# Windows Instructions

## Install Chocolatey
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

## Vagrant
If rsync is installed after vagrant, you might need to restart vagrant to make sure rsync is in vagrant's path.  
Make sure that rsytnc is added to the path. I need to verify if the chocolatey package does this.

```
choco install rsync -a
choco install vagrant -y
choco install virtualbox - y
```


# Linux
In progress...  
Based on - https://www.vagrantup.com/docs/installation
```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install vagrant
```