job "gitlab" {
  datacenters = ["dc1"]
  type = "service"
  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  group "gitlab-server" {
    network {
      port "web" {
          static = 8081
          to = 80
      }
    }

    service {
      name = "gitlab"
      port = "web"
    }

    task "container" {
      driver = "docker"
      config {
        ports = ["web"]
        image = "gitlab/gitlab-ce"
      }

      resources {
        memory = 4096
      }
    }
  }
 
}
