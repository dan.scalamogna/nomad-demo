pushd ./src/bootstrap
######################## Consul ############################
echo "installing consul..."
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install consul
sudo apt-get update && sudo apt-get -y install consul
consul -autocomplete-install
complete -C /usr/bin/consul consul

# create consul user 
sudo useradd --system --home /etc/consul.d --shell /bin/false consul

# create directoires and assign perms
sudo mkdir --parents /opt/consul
sudo chown --recursive consul:consul /opt/consul
sudo mkdir --parents /etc/consul.d
sudo chown --recursive consul:consul /etc/consul.d

# copy config files
sudo cp consul/consul.hcl /etc/consul.d/consul.hcl
sudo chmod 640 /etc/consul.d/consul.hcl
sudo consul validate /etc/consul.d/consul.hcl

# creat systemd config file 
sudo mv /consul/consul.service /usr/lib/systemd/system/consul.service

echo "starting consul..."
sudo systemctl enable consul
sudo systemctl start consul
sudo systemctl status consul
sleep 5
consul members