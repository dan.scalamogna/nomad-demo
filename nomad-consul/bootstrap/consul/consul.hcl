datacenter = "dc1"
data_dir = "/opt/consul"
server = true
bootstrap_expect = 1
client_addr = "0.0.0.0"
ui = true
bind_addr = "{{GetInterfaceIP \"eth0\"}}"

ports {
  grpc = 8502
}

connect {
  enabled = true
}

enable_central_service_config = true