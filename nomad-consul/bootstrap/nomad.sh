#!/bin/bash

pushd ./src/infrastructure

######################## Nomad Binaries ########################
echo "installing nomad..."
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install nomad
echo "starting nomad..."

# consul plugins
curl -L -o cni-plugins.tgz "https://github.com/containernetworking/plugins/releases/download/v1.0.0/cni-plugins-linux-$( [ $(uname -m) = aarch64 ] && echo arm64 || echo amd64)"-v1.0.0.tgz
sudo mkdir -p /opt/cni/bin
sudo tar -C /opt/cni/bin -xzf cni-plugins.tgz

# copy config
sudo mkdir --parents /opt/nomad
sudo cp nomad/nomad.service /etc/systemd/system/nomad.service
sudo mkdir --parents /etc/nomad.d
sudo chmod 700 /etc/nomad.d
sudo cp nomad/nomad.hcl /etc/nomad.d/nomad.hcl

sudo cp nomad/consul.conf /etc/sysctl.d/

sudo systemctl enable nomad
sudo systemctl start nomad
sudo systemctl status nomad
sleep 5s 
nomad agent-info