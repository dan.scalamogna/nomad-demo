job "batch-success" {
  datacenters = ["dc1"]
  type = "batch"
  group "batch-group" {
    count = 1
    reschedule {
      attempts = 0
      unlimited = false
    }
    restart {
      attempts = 0
      mode = "fail"
    }
    task "run-script" {
      driver = "exec"
      config {
        command = "/bin/bash"
        args    = ["./local/run.sh"]
      }
      template {
          data = file("./batch/run.sh")
          destination = "./local/run.sh"
      }
    }
  }
}